import config from './config';
import jwt from 'jsonwebtoken';

const secretKey = config.SECRET_KEY;
export const createAuthToken = (payload: string | object, secretKey: string, expiration: number) => {
	return jwt.sign(payload, secretKey);
};

export const verifyAuthToken = (token: string) => {
	return jwt.verify(token, secretKey, (err, decode) => {
		return decode !== undefined ? decode : err;
	});
}

export const checkForAuthToken = async (req: any, res: any, next: () => void) => {
	if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
		const status = 401;
		const message = 'Error in authorization format';
		res.status(status).json({status, message});
		return;
	}

	try {
		verifyAuthToken(req.headers.authorization.split(' ')[1]);
		next();
	} catch(error) {
		const status = 401;
		const message = 'Error access_token is revoked';
		res.status(status).json({status, message});
	}
};

export const normalizeRoomName = (roomName: string) => {
	return '#' + roomName.toLowerCase().replaceAll(" ", "-");
}
