import * as dotenv from 'dotenv';

dotenv.config();
const config = {
  CLIENT_ID: process.env.CLIENT ?? '',
  CLIENT_SECRET: process.env.CLIENT_KEY ?? '',
  SECRET_KEY: process.env.SECRET_KEY ?? '',
  EXPIRES_IN: process.env.EXPIRES_IN ?? '60',
  PORT: process.env.PORT ?? 5000,
};

export default config;
