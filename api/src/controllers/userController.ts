import {AppDataSource} from "../data-source";
import { User } from "../entities/User";
import { ResponseInterface } from "../interfaces/ResponseInterface";
import { UserInterface } from "../interfaces/UserInterface";

export const create = async(payload: any): Promise<ResponseInterface> => {
    try{
        console.log(payload);
        const existingUser = await AppDataSource.manager.findOne(User,
          {
              where: [{username: payload.username}],
              relations: {
                  joinedRooms: true,
              }
          });
        if (existingUser) {
            return {
                success: true,
                code: 200,
                reason: "user logged in",
                data: existingUser
            }
        }
        const user = new User();
        user.username = payload.username;
        await user.save();

        return {
            success: true,
            data: user
        };

    } catch (err: any) {
        throw new Error(err);
    }
}

export const getById = async(userId: number): Promise<ResponseInterface> => {
    const userData = await AppDataSource.manager.findOne(User, {
        where: [{id: userId}],
        relations: {
            joinedRooms: true
        }
    });
    if(!userData) {
        return {
            success: false,
            code: 400,
            reason: "couldn't find user"
        }
    }

    const user: UserInterface = {
        id: userData.id,
        username: userData.username,
        createdRooms: userData.createdRooms,
        joinedRooms: userData.joinedRooms,
        messages: []
    }

    return {
        success: true,
        data: user
    }
}

export const getAll = async(): Promise<ResponseInterface> => {
    const usersList = await AppDataSource.manager.find(User);
    return {
        success: true,
        data: usersList
    }
}
