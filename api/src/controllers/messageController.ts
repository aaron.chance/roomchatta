import { AppDataSource } from "../data-source";
import { User } from "../entities/User";
import { ResponseInterface } from "../interfaces/ResponseInterface";
import { Room } from "../entities/Room";
import { Message } from "../entities/Message";
import { normalizeRoomName } from "../utilities/helpers";

export const create = async(payload: any): Promise<ResponseInterface> => {
    const fromUser = await AppDataSource.manager.findOneBy(User, {id: payload.fromUser});
    const roomName = normalizeRoomName(payload.toRoom);
    const roomExists = await AppDataSource.manager.findOneBy(Room, {roomName: roomName});
    if (!fromUser) {
        return {
            success: false,
            code: 400,
            reason: "one or more users does not exist",
        }
    }

    let room;
    if (!roomExists) {
        room = new Room();
        room.roomName = roomName;
        room.owner = fromUser;
        room.isPublic = false;
        room.isDirect = true;
        room.participants = [];
        room.participants.push(fromUser);
        if(payload.isDirectMessage) {
            const toUser = await AppDataSource.manager.findOneBy(User, {username: payload.toRoom});
            if(!toUser) {
                return {
                    success: false,
                    code: 404,
                    reason: "user doesn't exist"
                }
            }
            room.participants.push(toUser)
        }
        await AppDataSource.manager.save(room)

    } else {
        room = roomExists;
    }

    const message = new Message();
    message.room = room;
    message.sendingUser = fromUser;
    message.text = payload.text;
    await message.save();

    return {
        success: true,
        reason: "message sent successfully",
        data: message
    }
}
