import {AppDataSource} from "../data-source";
import { User } from "../entities/User";
import {ResponseInterface} from "../interfaces/ResponseInterface";
import {Room} from "../entities/Room";
import {Message} from "../entities/Message";


export const getAll = async(): Promise<ResponseInterface> => {
    const roomList = await AppDataSource.manager.find(Room, {
        where: [{isPublic: true}]
    });
    return {
        success: true,
        reason: "Rooms fetched",
        data: roomList
    }
};

export const create = async(payload: any): Promise<ResponseInterface> => {

    console.log(payload);
    let roomName = '#' + payload.roomName.toLowerCase().replaceAll(" ", "-");
    const roomExists = await AppDataSource.manager.findOne(Room, {
        where: [{ roomName: roomName }],
    });
    const owner = await AppDataSource.manager.findOneBy(User, {id: payload.owner});
    if (!owner) {
        return {
            success: false,
            code: 404,
            reason: "User Id not found"
        }
    }

    if (roomExists) {
        return {
            success: false,
            code: 400,
            reason: "Room with that name already exists",
        }
    }
    const room = new Room();
    room.roomName = roomName;
    room.isPublic = payload.isPublic;
    room.isDirect = payload.isDirect;
    room.owner = owner;
    await room.save();

    return {
        success: true,
        reason: "room created successfully",
        data: room
    }
}

export const joinRoom = async(payload: any): Promise<ResponseInterface> => {

    const user = await AppDataSource.manager.findOne(User, {
        where: [{id: payload.joiningUser}],
        relations: {
            joinedRooms: true
        }
    });
    const room = await AppDataSource.manager.findOne(Room, {
        where: [{id: payload.room}],
        relations: {
            owner: true
        }
    });

    if (!user || !room) {
        return {
            success: false,
            code: 404,
            reason: "user or room does not exist"
        }
    }
    if (!room.isPublic && !payload.invited) {
        return {
            success: false,
            code: 403,
            reason: "this is a private, invite-only room"
        }
    }
    if (!room.isPublic && !room.isDirect && payload.invited) {
        if (room.owner.id !== payload.invitedBy) {
            return {
                success: false,
                code: 403,
                reason: "action not allowed"
            }
        }
    }

    const joinedRooms = user.joinedRooms;
    const userAlreadyJoined = joinedRooms.find((room) => room.id === payload.room);
    if (userAlreadyJoined) {
        return {
            success: false,
            code: 400,
            reason: "user already joined this room"
        }
    }

    user.joinedRooms.push(room);
    await user.save();

    return {
        success: true,
        reason: 'joined room ' + room.roomName,
        data: room
    }
}

export const getMessages = async(id: number): Promise<ResponseInterface> => {

    const room = await AppDataSource.manager.findOne(Room, {
        where: [{id: id}],
        relations: {
            messages: true
        }
    })
    if (!room) {
        return {
            success: false,
            code: 404,
            reason: "room not found"
        }
    }
    return {
        success: true,
        reason: `messages fetched for room ${id}`,
        data: room.messages
    }
}
