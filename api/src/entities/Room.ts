import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, OneToMany, ManyToMany, JoinTable} from "typeorm"
import { User } from "./User";
import {Message} from "./Message";

@Entity()
export class Room extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    roomName: string;

    @Column()
    isPublic: boolean;

    @Column()
    isDirect: boolean;

    @ManyToOne(() => User, (user) => user.createdRooms)
    owner: User;

    @ManyToMany(() => User, (user) => user.joinedRooms, { cascade: true })
    @JoinTable({
        name: "room_participants",
        joinColumn: {
            name: "roomId",
            referencedColumnName: "id"
        },
        inverseJoinColumn: {
            name: "userId",
            referencedColumnName: "id"
        }
    })
    public participants: User[];

    @OneToMany(() => Message, (message) => message.room)
    messages: Message[];

}
