import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, ManyToMany} from "typeorm"
import {Room} from "./Room";
import {Message} from "./Message";

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number

    @Column({ unique: true })
    username: string

    @OneToMany(() => Room, (room) => room.owner, {
        eager: true
    })
    createdRooms: Room[]

    @ManyToMany(() => Room, (room) => room.participants)
    joinedRooms: Room[]

    @OneToMany(() => Message, (message) => message.sendingUser)
    sentMessages: Message[]
}
