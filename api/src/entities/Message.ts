import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, CreateDateColumn} from "typeorm"
import { User } from "./User";
import { Room } from "./Room";

@Entity()
export class Message extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Room, (room) => room.messages)
    room: Room

    @ManyToOne(() => User, (user) => user.sentMessages, {eager: true})
    sendingUser: User

    @Column()
    text: string;

    @CreateDateColumn()
    sent_at: Date;
}
