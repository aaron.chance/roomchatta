export interface ResponseInterface {
    success: boolean;
    code?: number;
    reason?: string;
    data?: any;
}
