import {User} from "../entities/User";
import {Message} from "../entities/Message";

export interface MessageInterface {
    id: number;
    owner: User;
    isPublic: boolean;
    isDirect: boolean;
    participants: User[];
    messages: Message[];
    text: string;
}
