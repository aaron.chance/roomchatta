import {Room} from "../entities/Room";
import {User} from "../entities/User";

export interface messageInterface {
    id: number;
    room: Room;
    sendingUser: User;
    text: string;
    created_at: Date
}
