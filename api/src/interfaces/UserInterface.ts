import {Message} from "../entities/Message";
import {Room} from "../entities/Room";

export interface UserInterface {
    id: number;
    username: string;
    createdRooms: Room[];
    joinedRooms: Room[];
    messages: Message[];
}
