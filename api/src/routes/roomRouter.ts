import { Router, Request, Response } from 'express';
import * as roomController from "../controllers/roomController";

const roomRouter = Router();

roomRouter.get('/', async (req: Request, res: Response, next) => {
  const result = await roomController.getAll();
  if (result.success) {
    res
      .status(200)
      .json({
        success: true,
        message: result.reason,
        rooms: result.data
      });
  }
})

roomRouter.post('/create', async (req: Request, res: Response, next) => {
    const payload = req.body;
    console.log(payload);
    const result = await roomController.create(payload);
    if (result.success) {
        res
            .status(200)
            .json({
                success: true,
                message: result.reason,
                room: result.data
            });
    } else {
        res
            .status(result.code ?? 400)
            .json({
                success: false,
                message: result.reason
            })
    }

});

roomRouter.post('/join', async (req: Request, res: Response, next) => {
    const payload = req.body;
    console.log(payload);
    const result = await roomController.joinRoom(payload);
    if (result.success) {
        res
            .status(200)
            .json({
                success: true,
                message: result.reason,
                room: result.data
            });
    } else {
        res
            .status(result.code ?? 400)
            .json({
                success: false,
                message: result.reason
            })
    }

});
roomRouter.get('/:id/messages', async (req: Request, res: Response, next) => {
  const id = Number(req.params.id);

  const result = await roomController.getMessages(id);
  if (result.success) {
    res
      .status(200)
      .json({
        success: true,
        message: "messages loaded",
        messages: result.data
      })
  } else {
    res
      .status(result.code ?? 400)
      .json({
        success: false,
        message: result.reason
      })

  }
});
export default roomRouter;
