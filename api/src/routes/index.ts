import { Router } from 'express';
import userRouter from "./userRouter";
import messageRouter from "./messageRouter";
import roomRouter from "./roomRouter";

const router = Router();

router.use('/users', userRouter);
router.use('/messages', messageRouter);
router.use('/rooms', roomRouter);
export default router;
