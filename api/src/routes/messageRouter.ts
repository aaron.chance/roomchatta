import { Router, Request, Response } from 'express';
import * as messageController from "../controllers/messageController";

const messageRouter = Router();

messageRouter.post('/send', async (req: Request, res: Response, next) => {
   const payload = req.body;
   const result = await messageController.create(payload);
   if (result.success) {
       res
           .status(200)
           .json({
               success: true,
               message: result.data
           });
   }
});

export default messageRouter;
