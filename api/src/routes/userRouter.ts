import { Router, Request, Response } from 'express';
import * as userController from '../controllers/userController';

const userRouter = Router();

userRouter.get('/', async(req: Request, res: Response) => {
  const result = await userController.getAll();
  if (result.success) {
    res
      .status(200)
      .json({
        success: true,
        message: "user list loaded",
        users: result.data
      })
  }
})

userRouter.get('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id);

    const result = await userController.getById(id);

    return res.status(200).send(result);
});

userRouter.post('/create', async (req: Request, res: Response, next) => {
    try {
        console.log(req);
        const payload = req.body;
        console.log("payload", payload);
        const result = await userController.create(payload);
        console.log(result);
        if (result.success) {
            res
                .status(200)
                .json({
                    success: true,
                    user: result.data,
                });
        } else {
            res
                .status(result.code ?? 400)
                .json({
                    success: false,
                    message: result.reason
                });
        }
    } catch (error) {
        console.error('ERROR: ', error);
        next(error);
        res
            .status(500)
            .json({
                success: false,
                message: "something went wrong"
            });
    }

});

export default userRouter;
