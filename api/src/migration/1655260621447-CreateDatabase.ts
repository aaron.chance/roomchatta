import { MigrationInterface, QueryRunner } from "typeorm";

export class updatedTables1655260621447 implements MigrationInterface {
    name = 'updatedTables1655260621447'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`room\` (\`id\` int NOT NULL AUTO_INCREMENT, \`roomName\` varchar(255) NOT NULL, \`isPublic\` tinyint NOT NULL, \`isDirect\` tinyint NOT NULL, \`ownerId\` int NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`user\` (\`id\` int NOT NULL AUTO_INCREMENT, \`username\` varchar(255) NOT NULL, UNIQUE INDEX \`IDX_78a916df40e02a9deb1c4b75ed\` (\`username\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`message\` (\`id\` int NOT NULL AUTO_INCREMENT, \`text\` varchar(255) NOT NULL, \`sent_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`roomId\` int NULL, \`sendingUserId\` int NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`room_participants\` (\`roomId\` int NOT NULL, \`userId\` int NOT NULL, INDEX \`IDX_e5a9c5cf868f51dabb972930d4\` (\`roomId\`), INDEX \`IDX_2fffd7766a5c57a31e8e81c1b4\` (\`userId\`), PRIMARY KEY (\`roomId\`, \`userId\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`room\` ADD CONSTRAINT \`FK_65283be59094a73fed31ffeee4e\` FOREIGN KEY (\`ownerId\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`message\` ADD CONSTRAINT \`FK_fdfe54a21d1542c564384b74d5c\` FOREIGN KEY (\`roomId\`) REFERENCES \`room\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`message\` ADD CONSTRAINT \`FK_1bebc9c3af2e0e19867538a1fc9\` FOREIGN KEY (\`sendingUserId\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`room_participants\` ADD CONSTRAINT \`FK_e5a9c5cf868f51dabb972930d41\` FOREIGN KEY (\`roomId\`) REFERENCES \`room\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`room_participants\` ADD CONSTRAINT \`FK_2fffd7766a5c57a31e8e81c1b40\` FOREIGN KEY (\`userId\`) REFERENCES \`user\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`room_participants\` DROP FOREIGN KEY \`FK_2fffd7766a5c57a31e8e81c1b40\``);
        await queryRunner.query(`ALTER TABLE \`room_participants\` DROP FOREIGN KEY \`FK_e5a9c5cf868f51dabb972930d41\``);
        await queryRunner.query(`ALTER TABLE \`message\` DROP FOREIGN KEY \`FK_1bebc9c3af2e0e19867538a1fc9\``);
        await queryRunner.query(`ALTER TABLE \`message\` DROP FOREIGN KEY \`FK_fdfe54a21d1542c564384b74d5c\``);
        await queryRunner.query(`ALTER TABLE \`room\` DROP FOREIGN KEY \`FK_65283be59094a73fed31ffeee4e\``);
        await queryRunner.query(`DROP INDEX \`IDX_2fffd7766a5c57a31e8e81c1b4\` ON \`room_participants\``);
        await queryRunner.query(`DROP INDEX \`IDX_e5a9c5cf868f51dabb972930d4\` ON \`room_participants\``);
        await queryRunner.query(`DROP TABLE \`room_participants\``);
        await queryRunner.query(`DROP TABLE \`message\``);
        await queryRunner.query(`DROP INDEX \`IDX_78a916df40e02a9deb1c4b75ed\` ON \`user\``);
        await queryRunner.query(`DROP TABLE \`user\``);
        await queryRunner.query(`DROP TABLE \`room\``);
    }

}
