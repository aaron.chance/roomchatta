import { AppDataSource } from "./data-source"
import cors from 'cors';
import express from 'express';
import bodyParser from 'body-parser';
import * as helpers from './utilities/helpers';
import config from './utilities/config';
import routes from './routes';

const { CLIENT_ID, CLIENT_SECRET, SECRET_KEY } = config;
const expiration = parseInt(config.EXPIRES_IN, 10) * 60 * 24;
const app = express();
const port = config.PORT;
const allowedOrigins = ['http://localhost:8080'];
const options = {
  origin: allowedOrigins,
};

AppDataSource.initialize();
app.use(cors(options));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/api/v0/token', (req, res) => {
  const { client_secret: clientSecret, client_id: clientId, scope } = req.body;
  const scopes = scope.split(' ');

  if (clientId === CLIENT_ID && clientSecret === CLIENT_SECRET) {
    const createdAt = Date.now();
    const expiresAt = new Date();
    expiresAt.setHours(expiresAt.getHours() + expiration);
    const authToken = helpers.createAuthToken({
      token_type: 'access_token',
      scope: scopes,
      iss: 'L1',
      aud: clientId,
      sub: clientId,
      nbf: createdAt,
      exp: expiresAt,
      iat: createdAt,
    }, SECRET_KEY, expiration);
    res.status(200).send({
      access_token: authToken,
      token_type: 'Bearer',
      expires_in: expiresAt,
    });
  } else {
    res.status(403).send();
  }
});

app.use('/api/v0', routes);
app.listen(port);
