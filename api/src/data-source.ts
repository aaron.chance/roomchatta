import "reflect-metadata"
import { DataSource } from "typeorm"

export const AppDataSource = new DataSource({
    type: 'mysql',
    host: 'roomchatta-db',
    port: 3306,
    username: 'roomchatta',
    password: 'roomchatta123',
    database: 'roomchatta',
    synchronize: false,
    logging: true,
    entities: [__dirname + "/entities/*.ts"],
    migrations: [__dirname + "/migration/*.{js,ts}"],
    subscribers: [],
})
