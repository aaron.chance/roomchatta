import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import * as React from 'react';
import {UserInterface} from '../interfaces/UserInterface';

interface LoginProps {
  handleLogin: (user: UserInterface) => void
}

export const LoginForm = (props: LoginProps) => {
  const { handleLogin } = props;
  const baseUrl = process.env.API_BASE_URL;
  const navigate = useNavigate();

  const handleFormSubmit = async (event: any) => {
    event.preventDefault();
    const formData = {
      username: event.target.username.value,
    };
    const res = await fetch(`${baseUrl}/users/create`, {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const resJson = await res.json();
    if (resJson.success) {
      handleLogin(resJson.user);
    }
  };
  return (
    <>
      <h1>Welcome To RoomChatta</h1>
      <p>A chat app based around rooms</p>
      <form onSubmit={handleFormSubmit}>
        <p>Enter your username to login:</p>
        <input type="text" name="username" />
        <button type="submit">Login</button>
      </form>
    </>
  );
};
