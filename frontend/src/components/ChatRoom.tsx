import * as React from 'react';
import { UserInterface } from '../interfaces/UserInterface';
import { arrayUnique } from '../utilities/helpers';
import './style.css';

interface ChatRoomProps {
  user: UserInterface
}

export const ChatRoom = (props: ChatRoomProps) => {
  const baseUrl = process.env.API_BASE_URL;
  const { user } = props;
  const messageTextValue = React.useRef<any>({});
  const [joinedRooms, setJoinedRooms] = React.useState<any>([]);
  const [userList, setUserList] = React.useState<any>([]);
  const [activeRoom, setActiveRoom] = React.useState<any>({});
  const [publicRooms, setPublicRooms] = React.useState<any>([]);
  const [activeRoomId, setActiveRoomId] = React.useState<number>(0);
  const [messagesList, setMessagesList] = React.useState<any>([]);
  React.useEffect(() => {
    const fetchData = async () => {
      const usersRes = await fetch(`${baseUrl}/users/`, {
        method: 'GET',
      });
      const usersResJson = await usersRes.json();
      if (usersResJson.success) {
        setUserList(usersResJson.users);
      }
      const roomsRes = await fetch(`${baseUrl}/rooms/`, {
        method: 'GET',
      });
      const roomResJson = await roomsRes.json();
      if (roomResJson.success) {
        setPublicRooms(roomResJson.rooms)
      }
    };

    // call the function
    fetchData()
      // make sure to catch any error
      .catch(console.error);
  }, []);

  React.useEffect(() => {
    if(user.joinedRooms && user.createdRooms) {
      const combinedRooms = user.joinedRooms.concat(user.createdRooms);
      const key = 'id';

      const roomsList = [...new Map(combinedRooms.map((item) => [item[key], item])).values()];
      console.log(combinedRooms, roomsList);
      setJoinedRooms(roomsList);
    }
  }, [user]);

  React.useEffect(() => {
    if (activeRoomId !== 0) {
      const interval = setInterval(async () => {
        const res = await fetch(`${baseUrl}/rooms/${activeRoomId}/messages`, {
          method: 'GET',
        });
        const resJson = await res.json();
        if (resJson.success) {
          setMessagesList(resJson.messages);
        }
      }, 3000);
      return () => clearInterval(interval);
    }
  }, [activeRoomId]);

  const changeActiveRoom = async (roomId: number, roomName: string, isDirect: boolean) => {
    const alreadyJoined = joinedRooms.find((room: any) => room.id === roomId);
    if(!alreadyJoined) {
      const joinRoomRes = await fetch(`${baseUrl}/rooms/join`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          room: roomId,
          joiningUser: user.id,
        }),
      });
      const joinRoomJson = await joinRoomRes.json();
      if (joinRoomJson.success) {
        const joinRoomList = [...joinedRooms];
        joinRoomList.push(joinRoomJson.room);
        setJoinedRooms(joinRoomList);
      }
    }
    const res = await fetch(`${baseUrl}/rooms/${roomId}/messages`, {
      method: 'GET',
    });
    const resJson = await res.json();
    if (resJson.success) {
      setMessagesList(resJson.messages);
      setActiveRoom(roomName);
      setActiveRoomId(roomId);
    }
  };

  const createNewRoom = async(event: any) => {
    event.preventDefault();
    const roomName = event.target.roomName.value;
    const res = await fetch(`${baseUrl}/rooms/create`, {
      method: 'POST',
      body: JSON.stringify({
        roomName,
        owner: user.id,
        isPublic: true,
        isDirect: false,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const resJson = await res.json();

    if (resJson.success) {
      const joinedRoomList = [...joinedRooms];
      joinedRoomList.push(resJson.room);
      setJoinedRooms(joinedRoomList);
    }
  };

  const createNewMessage = async(event: any) => {
    event.preventDefault();
    const newMessageText = event.target.messageText.value;
    console.log(activeRoom);
    const formData = {
      fromUser: user.id,
      toRoom: activeRoom.replaceAll('#', ''),
      isDirect: false,
      text: newMessageText,
    };

    const res = await fetch(`${baseUrl}/messages/send`, {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    const resJson = await res.json();
    if (resJson.success) {
      messageTextValue.current.value = '';
    }
  };

  return (
    <div className="chatRoom">
      <div className="sidebar">
        <div>
          Joined Rooms List
          <ul>
            {joinedRooms.map((room: any) => (
              <li key={room.id} onClick={() => changeActiveRoom(room.id, room.roomName,false)}>{room.roomName}</li>
            ))}
          </ul>
        </div>
        <div>
          Public Rooms List
          <ul>
            {publicRooms.map((room:any) => (
              <li key={room.id} onClick={() => changeActiveRoom(room.id, room.roomName, false)}>{room.roomName}</li>
            ))}
          </ul>
        </div>
        <div>
          User List
          <ul>
            {userList.map((item: any) => (
              <li key={item.id}>{item.username}</li>
            ))}
          </ul>
        </div>
        <form onSubmit={createNewRoom}>
          <label htmlFor="roomName">
            Create Room
            <br />
            <input type="text" id="roomName" name="roomName" />
          </label>
          <br />
          <button type="submit">Create Room</button>
        </form>
      </div>
      <div className="mainArea">
        <div className="textDisplay">
          {messagesList.map((message: any) => (
            <li key={message.id}>
              <p>{message.sendingUser.username}: </p>
              <p>{message.text}</p>
            </li>
          ))}
        </div>
        <div className="textEntry">
          <form action="" onSubmit={createNewMessage}>
            <input ref={messageTextValue} type="text" name="messageText" id="messageText" />
            <button type="submit">Send</button>
          </form>
        </div>
      </div>
    </div>
  );
};
