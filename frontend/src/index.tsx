import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './styles/styles.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from "./pages/Home";

const mountNode = document.getElementById('root');
ReactDOM.render(
  <Router>
    <div className="container-fluid">
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    </div>
  </Router>,
  mountNode,
);
