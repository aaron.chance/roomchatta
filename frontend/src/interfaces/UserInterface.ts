export interface UserInterface {
  id: number;
  username: string;
  createdRooms: any[];
  joinedRooms: any[];
  messages: any[];
}
