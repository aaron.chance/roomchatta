import * as React from 'react';
import { LoginForm } from '../components/LoginForm';
import { ChatRoom } from '../components/ChatRoom';
import { UserInterface } from '../interfaces/UserInterface';

const Home = () => {
  const defaultUser: UserInterface = {
    id: 0,
    username: '',
    createdRooms: [],
    joinedRooms: [],
    messages: [],

  };
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [user, setUser] = React.useState<UserInterface>(defaultUser);

  const handleLogin = (userData: UserInterface) => {
    setUser(userData);
    setIsLoggedIn(true);
  };
  console.log(isLoggedIn);
  return (
    <>
      {!isLoggedIn && (
        <LoginForm
          handleLogin={handleLogin}
        />
      )}
      {isLoggedIn && (
        <ChatRoom
          user={user}
        />
      )}
    </>
  );
};

export default Home;
