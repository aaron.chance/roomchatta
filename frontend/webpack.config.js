const dotenv  = require('dotenv')
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Dotenv = require('dotenv-webpack');

require('dotenv').config({ // it puts the content to the "process.env" var. System vars are taking precedence
  path: '.env',
});
const config = {
  entry: './src/index.tsx',
  mode: process.env.NODE_ENV || "development",
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/
    },
    {
        test: /\.ts(x)?$/,
        loader: 'ts-loader',
        exclude: /node_modules/
    },
    {
        test: /\.(css|scss)$/,
        use: ["style-loader", "css-loader"],
    },
    ]
  },
  devServer: {
    historyApiFallback: {disableDotRule: true},
    'static': {
        directory: './dist'
    }
},
  plugins: [
    new HtmlWebpackPlugin({
        template: "./public/index.html",
    }),
    new Dotenv(),
],
  resolve: {
    modules: [__dirname, "src", "node_modules"],
    extensions: [
      '.tsx',
      '.ts',
      '.js'
    ]
  }
};

module.exports = config;
