# RoomChatta

Hello, thanks for reviewing this. This is my submission for the Guild Education tech review.

I've built a chat app that allows users to "login" and create and then join public rooms. they can then post messages
to those rooms and any other connected users will receive the message within ~5 seconds. I had a lot of ideas for my 
implementation but simply ran out of time to flesh them all out; I wanted to implement a room based system to demonstrate
future scalability but as I got further I had to cut back implementation significantly.

## Setup
The front and backend are both configured with docker and makefiles. it's probably best to first `cd` into each directory
and run `make init` to get the docker containers built. Then you can run `docker-compose up` from the root directory. 
Fair warning, I haven't fully tested this part thoroughly, but using `docker-compose up` I've otherwise had no issues 
throughout development.

I've also added the .env files. I wouldn't typically do this in a production level repository, but for the sake of ease
of setup for you, I want to ensure you have all the configuration pieces necessary to get running.

## API
The API is written using node, express, and typescript. I used TypeORM for the database interaction and MySQL as my storage
medium. I created entities(models) for User, Room, Message, and a lookup table to connect users to rooms. The most difficult
portion here was learning TypeORMs DSL for interacting with entities and relationships. You might notice further functionality
implemented on the API side than I was able to connect to with on the frontend.

There are migrations that can be run using the command:

`docker-compose run roomchatta-api npx typeorm-ts-node-esm migration:run -d ./src/data-source.ts`

This should allow you to get the database tables configured and running properly.

## Frontend
I used React for the front end along with typescript. I chose to go with a no-frills layout, as requested, only adding 
enough styling to be minimally functional. Through the frontend you can "login" (which either creates a user or passes
back a user that matches the string username entered in the box) and then once logged in can see a list of public rooms.

New public rooms can be created through the form on the left. When a user clicks on a room in the public room list,
they will be joined to that room if they aren't already. This allows them to see all previous and ongoing history as well
as send messages to the room.

The message display is updated using setInterval. It wasn't my first design choice, I would have preferred to implement 
sockets, but it was one of the features I opted to cut for the sake of time. setInterval gets the job done and I tried
to limit it to within the component and clear the interval when the component is unmounted.
